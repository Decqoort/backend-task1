public class Task1 {
    public static void main(String[] args) {
        int gradeBook = 0x94F5;
        long phoneNumber = 89825657613L;
        int numberInGroup = 5;
        int numberForTask = (numberInGroup - 1) % 26 + 1;
        char charForTask = (char)((int)'A' + numberForTask);
        String lastDigitsBin = Integer.toBinaryString((int) (phoneNumber%100));
        String lastDigitsOct = Integer.toOctalString((int) (phoneNumber%10000));
        System.out.println("Зачетная книжка: " + gradeBook);
        System.out.println("Номер телефона: " + phoneNumber);
        System.out.println("2-ая: " + lastDigitsBin);
        System.out.println("8-ая: " + lastDigitsOct);
        System.out.println(numberForTask);
        System.out.println(charForTask);
    }
}
